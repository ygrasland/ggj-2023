using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyController : MonoBehaviour
{
    /// <summary>
    /// Move speed of this enemy.
    /// </summary>
    public float Speed
    {
        get => speed;
        set { speed = value; }
    }
    [SerializeField]
    private float speed = 2;

    /// <summary>
    /// The game board on which this enemy is moving.
    /// </summary>
    public Board GameBoard
    {
        get => gameBoard;
        set { GameBoard = value; }
    }
    [SerializeField]
    private Board gameBoard;

    /// <summary>Whever we need to select a new target location</summary>
    private bool needsTarget;

    /// <summary>Next destination</summary>
    private (int, int) targetCell;

    protected virtual void Start()
    {
        needsTarget = true;
    }

    protected virtual void FixedUpdate()
    {
        // Select a new target destination if we do not have one
        if (needsTarget)
        {
            var currentCell = gameBoard.PositionToCellIndex(transform.position);
            var alternatives = new List<(int, int)> {
                (currentCell.Item1 -1, currentCell.Item2),
                (currentCell.Item1 +1, currentCell.Item2),
                (currentCell.Item1, currentCell.Item2 -1),
                (currentCell.Item1, currentCell.Item2 +1),
            };
            while (alternatives.Count > 0)
            {
                var candidateIndex = Random.Range(0, alternatives.Count);
                var candidate = alternatives[candidateIndex];
                alternatives.RemoveAt(candidateIndex);
                if (!gameBoard.IsCellBlocked(candidate))
                {
                    targetCell = candidate;
                    needsTarget = false;
                    break;
                }
            }
        }

        if (!needsTarget)
        {
            // Try to move towards the target
            var estimatedMoveDistance = speed * Time.deltaTime;
            var targetPosition = gameBoard.CellIndexToCenterPosition(targetCell);
            // Assume a move in the XZ plane, Y=0 for all pivots, etc.
            var moveDirection = targetPosition - transform.position;
            if (moveDirection.sqrMagnitude <= estimatedMoveDistance * estimatedMoveDistance)
            {
                // We're close enough to teleport on the target location
                transform.position = targetPosition;
                needsTarget = true;
            }
            else
            {
                // Move towards the target
                transform.position += moveDirection.normalized * estimatedMoveDistance;
            }
        }
    }

    public void ReceiveAttack(PlayerWeapon source)
    {
        // All attacks are insta-kills for now.
        Destroy(gameObject);
    }
}
