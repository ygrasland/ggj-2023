using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DreamObstacle : MonoBehaviour
{
    /// <summary>
    /// The Dream reward which allows clearing this obstacle.
    /// If this reward has been earned previously, this object will self-destruct on Awake.
    /// </summary>
    public string RequiredDreamReward;

    protected virtual void Awake()
    {
        if (RequiredDreamReward != null)
        {
            // Self-destruct if our corresponding dream is complete.
            if (Game.Instance.IsDreamComplete(RequiredDreamReward))
            {
                Destroy(gameObject);
            }
        }
        else {
            Debug.LogError("No Dream reward is identified which would allow clearing this obstacle");
        }
    }
}
