using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenUI : MonoBehaviour
{	
	public void ShowBriefing()
	{
		briefingPos = ShowHiddenFrame("BriefingFrame");
	}
	
	public void FinishBriefing()
	{
		RestoreHiddenFrame("BriefingFrame", briefingPos);
		StartGame();
	}

    public void ShowCredits()
    {
		creditsPos = ShowHiddenFrame("CreditsFrame");
    }
	
    public void HideCredits()
    {
		RestoreHiddenFrame("CreditsFrame", creditsPos);
    }
	
    public void QuitGame()
    {
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}
	
	// Bring a hidden frame in front of the camera, return its former position
	private Vector3 ShowHiddenFrame(string tag) {
	    GameObject hiddenFrame = GameObject.FindWithTag(tag);
		Vector3 oldPos = hiddenFrame.transform.localPosition;
		hiddenFrame.transform.localPosition = new Vector3(0, 0, 0);
		return oldPos;
	}
	
	// Restore a hidden frame to its old position
	private void RestoreHiddenFrame(string tag, Vector3 oldPos) {
        GameObject.FindWithTag(tag).transform.localPosition = creditsPos;
	}
	
    private void StartGame()
    {
        Game.Instance.Reset();
        SceneManager.LoadScene("Scenes/Adventure");
    }
	
	private Vector3 creditsPos;
	private Vector3 briefingPos;
}
