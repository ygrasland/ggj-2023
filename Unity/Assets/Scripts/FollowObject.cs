using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pilots the camera to follow the player
/// </summary>
public class FollowObject : MonoBehaviour
{
    public Transform Target
    {
        get => target;
        set
        {
            target = value;
        }
    }
    [SerializeField]
    private Transform target;

    private Vector3 positionDelta;
    private Quaternion rotationDelta;

    public Space SimulationSpace
    {
        get => simulationSpace;
        set
        {
            if (simulationSpace!= value)
            {
                positionDelta = GetCurrentPositionDelta(value);
                rotationDelta = GetCurrentRotationDelta(value);
                simulationSpace = value;
            }
        }
    }
    [SerializeField]
    private Space simulationSpace = Space.World;

    protected virtual void Start()
    {
        positionDelta = GetCurrentPositionDelta(simulationSpace);
        rotationDelta = GetCurrentRotationDelta(simulationSpace);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (target == null)
        {
            Debug.Log("No aim: self-destructing");
            Destroy(this);
            return;
        }

        switch (simulationSpace)
        {
            case Space.World:
                transform.position = target.position + positionDelta;
                break;
            case Space.Self:
                transform.position = target.TransformPoint(positionDelta);
                transform.rotation = target.rotation * rotationDelta;
                break;
            default:
                throw new NotImplementedException();
        }
    }

    private Vector3 GetCurrentPositionDelta(Space space)
    {
        switch (space)
        {
            case Space.World:
                return transform.position - target.position;
            case Space.Self:
                return target.InverseTransformPoint(transform.position);
            default:
                throw new NotImplementedException();
        }
    }

    private Quaternion GetCurrentRotationDelta(Space space)
    {
        switch (space)
        {
            case Space.World:
                return Quaternion.identity;
            case Space.Self:
                return Quaternion.Inverse(target.rotation) * transform.rotation;
            default:
                throw new NotImplementedException();
        }
    }
}
