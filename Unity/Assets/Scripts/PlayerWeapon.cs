using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    /// <summary>
    /// Handles contacts with enemies
    /// </summary>
    protected virtual void OnTriggerEnter(Collider collider)
    {
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        var touchedObject = collider.gameObject;
        if (touchedObject.layer == enemyLayer)
        {
            var enemy = touchedObject.GetComponentInParent<EnemyController>();
            if (enemy != null)
            {
                enemy.ReceiveAttack(this);
            }
            else
            {
                Debug.LogWarning("Touched an object on the Enemy layer which does not have an EnemyController script.");
            }
        }
    }
}
