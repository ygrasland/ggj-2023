using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BedInteractor : MonoBehaviour, IInteractible
{
    public void Interact(GameObject player)
    {
        GetComponentInParent<Bed>().Activate();
    }
}
