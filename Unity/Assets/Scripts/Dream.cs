using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dream state tracker that knows which entities must still be interacted with
/// </summary>
public class Dream : MonoBehaviour
{
	// Reward ID used to track which dream we're talking about
    public string rewardId;
	// Text color to be used for objects that have already been interacted with
	public Color32 interactedTextColor = new Color32(0, 0, 128, 255);

	// List of dream entities and truth that they have been interacted with
	private Dictionary<DreamEntity, bool> entities = new Dictionary<DreamEntity, bool>();
	// Number of dream entities that must still be interacted with
	private uint remainingInteractions = 0;
	
	// Register an inner entity
	public void RegisterEntity(DreamEntity entity)
	{
		entities.Add(entity, false);
		remainingInteractions += 1;
	}
	
	// Record that an entity has been interacted with
	public void InteractEntity(DreamEntity entity)
	{
		if (!entities[entity]) {
			entities[entity] = true;
			remainingInteractions -= 1;
			if (remainingInteractions == 0) {
				if (rewardId != null)
				{
					Game.Instance.CompleteDreamScene(rewardId);
				}
				else
				{
					Debug.LogError("The reward id for this dream scene is not set.");
				}
			}
		}
	}
	
	/// <summary>
    /// Reinitializes the game progress to its initial state.
    /// </summary>
    public void Reset()
    {
		List<DreamEntity> interactedEntities = new List<DreamEntity>();
		foreach (var (entity, interacted) in entities)
		{
			if (interacted) {
				interactedEntities.Add(entity);
			}
		}
		foreach (var entity in interactedEntities) {
			entity.ResetInteraction();
			entities[entity] = false;
			remainingInteractions += 1;
		}
    }
}
