using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Script for a single dream entity (object, character).
//
// Requires a trigger box collider and a text canvas.
// Put me on the GameObject containing the BoxCollider.
// If the text canvas is a child it will be found
// automatically on Start, otherwise you need to specify
// it manually via the manualCanvasHost member.
//
public class DreamEntity : MonoBehaviour, IInteractible
{
	// Entity hosting the Canvas where the text is
	// You don't need to specify it if the Canvas is a
	// child of this GameObject.
	public MonoBehaviour manualCanvasHost = null;

	// Dream which this entity belongs to
	private Dream dream;
	// Text display associated with this entity
	private TextMeshProUGUI text;
	// Initial text color
	private Color32 initialTextColor;

    // Start is called before the first frame update
    public void Start()
    {
		// Determine where to look for the Canvas
		MonoBehaviour canvasHost = (manualCanvasHost == null) ? this : manualCanvasHost;
		
		// Locate the canvas and set up the ProximityActivator
		GameObject canvas = canvasHost.GetComponentInChildren<Canvas>(true).gameObject;
		ProximityActivator canvasActivator = gameObject.AddComponent<ProximityActivator>();
		canvasActivator.activableThing = canvas;

		// Locate the description text
		text = canvas.GetComponentInChildren<TextMeshProUGUI>(true);
		initialTextColor = text.color;

		// Find the dream state tracker and make ourselves known
        dream = GetComponentInParent<Dream>();
		dream.RegisterEntity(this);
    }
	
	// Interact is called when the player interacts with this entity with "e"
    public void Interact(GameObject player)
    {
		// Make it visible to the player that the entity has been interacted with
		text.color = dream.interactedTextColor;
		// TODO: More fancy effects (particles etc) if we find the time
		
		// Make the interaction known to the dream state tracker, possibly ending the dream
		dream.InteractEntity(this);
    }
	
	// Reset interaction-related state (method called by Dream, so no need to call it back)
	public void ResetInteraction()
	{
		text.color = initialTextColor;
	}
}
