using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBootstrap : MonoBehaviour
{
    public Game gamePrefab;

    protected virtual void Awake()
    {
        if (Game.Instance == null)
        {
            Debug.LogWarning($"Instantiating the {nameof(Game)} prefab since it was not present in the scene");
            Instantiate(gamePrefab);
        }
        Destroy(gameObject);
    }
}
