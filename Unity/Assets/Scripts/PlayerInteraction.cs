using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Seeks interactible objects in range and triggers them.
/// </summary>
public class PlayerInteraction : MonoBehaviour
{
    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)){
            float interactRange = 1f;
            Collider[] neighbours = Physics.OverlapSphere(transform.position, interactRange);
            foreach (Collider neighbour in neighbours) {
                if (neighbour.TryGetComponent(out IInteractible interactible))
                {
                    interactible.Interact(gameObject);
                }
            }
        }
    }
}
