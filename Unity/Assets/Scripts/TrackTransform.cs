using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes an object stick to another without creating a GameObject parenting.
/// </summary>
public class TrackTransform : MonoBehaviour
{
    public Transform Target
    {
        get => target;
        set
        {
            target = value;
        }
    }
    [SerializeField]
    private Transform target;

    public bool DestroyGameObjectWhenTargetDies
    {
        get => destroyGameObjectWhenTargetDies;
        set
        {
            destroyGameObjectWhenTargetDies = value;
        }
    }
    [SerializeField]
    private bool destroyGameObjectWhenTargetDies = true;

    protected virtual void Start()
    {
        TrackTarget();
    }

    protected virtual void Update()
    {
        TrackTarget();
    }

    private void TrackTarget()
    {
        if (target == null) {
            Destroy(destroyGameObjectWhenTargetDies? gameObject: this);
            return;
        }
        transform.position = target.position;
        transform.rotation = target.rotation;
        transform.localScale = target.localScale;
    }
}
