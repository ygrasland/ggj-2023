using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inquisition : MonoBehaviour, IInteractible
{
	// Army of bodyguards
	public GameObject guards;
	// "You win" screen
	public GameObject winScreen;
	
	// Canvas used to make the inquisitor speak
	private GameObject canvas;
	// Text display within this canvas
	private TextMeshProUGUI text;
	// How far along are we with the whole discussion thing
	// 0 = Just met
	// 1 = Saw header
	// 2 = Passed through sentence
	private uint state = 0;
	// Player controller handle
	private PlayerController controller;

    // Find the canvas and text display
    public void Start()
    {
		canvas = GetComponentInChildren<Canvas>(true).gameObject;
		text = canvas.GetComponentInChildren<TextMeshProUGUI>(true);
    }
	
	// Called when the player enters the interaction zone...
    protected virtual void OnTriggerEnter(Collider other)
    {
		// Only interact with the player
        if(other.GetComponentInParent<PlayerController>() == null) {
		    return;
        }
		
		// Show the text at the start
		if (state == 0) {
			canvas.SetActive(true);
			state += 1;
		}
    }
	
	// Interact is called when the player interacts with this entity with "e"
    public void Interact(GameObject player)
    {
		if (state == 1) {
			text.text = "Choisissez quel th�me\nlui est le plus cher!";
			state += 1;
		}
    }
	
	// Submit the player's choice
	public void SubmitResult(bool winning) {
		if (winning) {
			text.text = "Excellent choix!\nJe suis convaincu.";
			winScreen.SetActive(true);
		} else {
			text.text = "A la garde!\nC'est un imposteur!";
			guards.SetActive(true);
		}
	}
}
