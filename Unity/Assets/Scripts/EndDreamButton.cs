using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This debug script allows completing dreams early, before the actual level mechanic is implemented.
/// </summary>
public class EndDreamButton : MonoBehaviour, IInteractible
{
    public bool success;
    public string rewardId;

    public void Interact(GameObject player)
    {
        if (success)
        {
            if (rewardId != null)
            {
                Game.Instance.CompleteDreamScene(rewardId);
            }
            else
            {
                Debug.LogError("The reward id for this dream scene is not set.");
            }
        }
        else
        {
            Game.Instance.NavigateToAdventureScene();
        }
    }
}
