﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Something the player can interact with.
/// </summary>
public interface IInteractible
{
    /// <summary>
    /// Triggers an interaction from the player with this object
    /// </summary>
    public void Interact(GameObject player);
}
