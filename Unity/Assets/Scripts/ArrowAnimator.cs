using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAnimator : MonoBehaviour
{
    public float duration = 2;
    public float verticalAmplitude = 0.3f;
    public Transform target;

    void Update()
    {
        float animationRelativeTime = (Time.time - duration * Mathf.Floor(Time.time / duration)) / duration;
        target.localPosition = new Vector3(0, 0, Mathf.Cos(2 * 2*Mathf.PI * animationRelativeTime) * verticalAmplitude);
        target.localRotation = Quaternion.Euler(0, 0, 360 * animationRelativeTime);
    }
}
