﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ProximityActivator: MonoBehaviour
{
    [SerializeField]
    public GameObject activableThing;

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent< PlayerController>() != null)
        {
            SetPromptActive(true);
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.GetComponentInParent<PlayerController>() != null)
        {
            SetPromptActive(false);
        }
    }

    /// <summary>
    /// Show or hides the things that indicate to the player that he may use this.
    /// </summary>
    private void SetPromptActive(bool active)
    {
        if (activableThing)
        {
            activableThing.SetActive(active);
        }
        else
        {
            Debug.LogWarning("The thing to (de)activate is not set up on this component. This will do nothing.");
        }
    }
}
