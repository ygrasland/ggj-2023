using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class assumptions: The board is in the XZ plane and has no Y rotation.
/// </summary>
[RequireComponent(typeof(Grid))]
public class Board : MonoBehaviour
{
    private float cellSizeX;
    private float cellSizeY;

    protected virtual void Awake()
    {
        // Read the tile dimensions from the Grid component
        var tilemapGrid = GetComponent<Grid>();
        cellSizeX = tilemapGrid.cellSize.x;
        cellSizeY = tilemapGrid.cellSize.y;
    }

    /// <summary>
    /// Returns the index of the board cell at the specified location.
    /// </summary>
    public (int, int) PositionToCellIndex(Vector3 position)
    {
        return (
            cellSizeX > 0 ? Mathf.FloorToInt(position.x / cellSizeX) : 0,
            // Cell Y <=> Position Z - See class assumptions at the top of the file.
            cellSizeY > 0 ? Mathf.FloorToInt(position.z / cellSizeY) : 0
        );
    }

    /// <summary>
    /// Gets the position of the center of a given cell.
    /// </summary>
    public Vector3 CellIndexToCenterPosition((int, int) cellIndex)
    {
        return new Vector3 (
            cellSizeX * (cellIndex.Item1 + 0.5f),
            0,
            cellSizeX * (cellIndex.Item2 + 0.5f)
        );
    }

    /// <summary>
    /// Checks whever the specified cell is bloacked an inaccessible.
    /// This takes walls (elements on the "Blockers" layer) into account, but not mobile objects like the player or ennemies.
    /// </summary>
    public bool IsCellBlocked((int, int) cellIndex)
    {
        const float rayLength = 10;
        var rayOrigin = CellIndexToCenterPosition(cellIndex);
        rayOrigin.y = rayLength;
        var ray = new Ray(rayOrigin, Vector3.down);
        return Physics.Raycast(ray, rayLength, LayerMask.GetMask("Blockers"));
    }
}
