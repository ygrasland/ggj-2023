using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bed : MonoBehaviour
{
    /// <summary>
    /// Dream scene to which this bed allows travelling.
    /// </summary>
    public string TargetSceneName
    {
        get => targetSceneName;
        set
        {
            targetSceneName = value;
        }
    }
    [SerializeField]
    private string targetSceneName;

    protected virtual void Awake()
    {
        targetSceneName = string.IsNullOrEmpty(targetSceneName)? Game.Instance.GetNextDreamScene(): targetSceneName;
    }

    public void Activate()
    {
        if (targetSceneName != null)
        {
            Game.Instance.LaunchDreamScene(targetSceneName);
        }
        else
        {
            Debug.LogError("The destination scene for the bed travel is not set.");
        }
    }
}
