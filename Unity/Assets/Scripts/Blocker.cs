using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hides editor-time blocker meshes.
/// </summary>
public class Blocker : MonoBehaviour
{
    protected virtual void Start()
    {
        Destroy(GetComponentInChildren<MeshRenderer>());
        Destroy(GetComponentInChildren<MeshFilter>());
    }
}
