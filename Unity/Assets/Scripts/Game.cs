using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game Instance
    {
        get => instance;
    }
    public static Game instance;

    /// <summary>
    /// Identifiant de la sc�ne d'aventure.
    /// </summary>
    public static string AdventureSceneId => "Scenes/Adventure";

    [SerializeField]
    private List<string> allDreamScenes;

    private readonly List<string> remainingDreams = new ();
    private readonly List<string> earnedRewards = new();

    /// <summary>
    /// Dream scene which was last started and not yet finished.
    /// </summary>
    public string CurrentDreamScene { get; private set; }

    protected virtual void Awake()
    {
        if (instance == null)
        {
            Reset();
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("Duplicate Game prefab, the latest one will self-destruct");
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Reinitializes the game progress to its initial state.
    /// </summary>
    public void Reset()
    {
        earnedRewards.Clear();
        remainingDreams.Clear();
        foreach (string dreamScene in allDreamScenes)
        {
            remainingDreams.Add(dreamScene);
        }
        CurrentDreamScene = null;
    }

    /// <summary>
    /// M�morise le fait que le joueur a termin� un certain r�ve avec succ�s.
    /// </summary>
    public void AwardDream(string rewardId)
    {
        if (!earnedRewards.Contains(rewardId))
        {
            earnedRewards.Add(rewardId);
        }
    }

    /// <summary>
    /// Tests whever a given Dream reward has been earned
    /// </summary>
    public bool IsDreamComplete(string rewardIdentifier) => earnedRewards.Contains(rewardIdentifier);

    public string GetNextDreamScene()
    {
        return remainingDreams.Count > 0 ? remainingDreams[0] : null;
    }

    public void LaunchDreamScene(string targetSceneName)
    {
        if (remainingDreams.Contains(targetSceneName))
        {
            CurrentDreamScene = targetSceneName;
        }
        else
        {
            Debug.LogWarning("The requested dream scene is not part of the remaining dreams to launch." +
                "It will be launched nonetheless, but it will yield no rewards.");
        }
        SceneManager.LoadScene(targetSceneName);
    }

    /// <summary>
    /// Call this to award a dream to the player and go back to the adventure scene.
    /// </summary>
    public void CompleteDreamScene(string rewardId)
    {
        if (CurrentDreamScene != null)
        {
            AwardDream(rewardId);
            remainingDreams.Remove(CurrentDreamScene);
            CurrentDreamScene = null;
        }
        NavigateToAdventureScene();
    }

    /// <summary>
    /// Call this to go back to the adventure scene without awarding a new dream to the player.
    /// </summary>
    public void NavigateToAdventureScene()
    {
        SceneManager.LoadScene(AdventureSceneId);
    }
}
