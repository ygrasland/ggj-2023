using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingChoice : MonoBehaviour, IInteractible
{
	// Whether this choice means winning or losing
	public bool winning;
	
    public void Interact(GameObject player)
    {
		GetComponentInParent<Inquisition>().SubmitResult(winning);
    }
}
