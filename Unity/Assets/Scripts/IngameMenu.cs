using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenu : MonoBehaviour
{
    public GameObject menuUI;

    public void Resume()
    {
        menuUI.SetActive(false);
    }
    public void Quit()
    {
        SceneManager.LoadScene("Scenes/Title");
    }

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            menuUI.SetActive(!menuUI.activeSelf);
        }
    }
}
