using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public float maxSpeed = 5;
    public float attackDuration = 2.0f;

    private float? attackStartTime = null;

    public Animator weaponMount;
    public TrackTransform weaponPrefab;

    private Rigidbody _rigidbody;
    private TrackTransform currentWeapon = null;

    protected virtual void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void Update()
    {
        // Move
        var input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        var moveDirection = input.normalized;
        var moveSpeed = Mathf.Min(input.magnitude, 1) * maxSpeed;
        _rigidbody.velocity = moveDirection * moveSpeed;

        // Orientation
        SetOrientation(moveDirection, moveSpeed);

        // Attack
        if (attackStartTime.HasValue)
        {
            if (Time.time >= attackStartTime.Value + attackDuration)
            {
                Destroy(currentWeapon.gameObject);
                currentWeapon = null;
                attackStartTime = null;
            }
        }
        else if (Input.GetButtonDown("Fire1"))
        {
            currentWeapon = Instantiate(weaponPrefab);
            currentWeapon.Target = weaponMount.transform;
            attackStartTime = Time.time;
            weaponMount.SetTrigger("DoSlash");
        }
    }

    /// <summary>
    /// Handles contacts from enemies
    /// </summary>
    protected virtual void OnTriggerEnter(Collider collider)
    {
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        var touchedObject = collider.gameObject;
        if (touchedObject.layer == enemyLayer)
        {
            var enemy = touchedObject.GetComponentInParent<EnemyController>();
            if (enemy != null)
            {
                ReceiveAttack(enemy);
            }
            else
            {
                Debug.LogWarning("Touched an object on the Enemy layer which does not have an EnemyController script.");
            }
        }
    }

    private void SetOrientation(Vector3 direction, float moveSpeed)
    {
        if (moveSpeed > 0.1)
        {
            float angle =
                Mathf.Abs(direction.x) > Mathf.Abs(direction.z) ?
                direction.x > 0 ? 90 : -90 :
                direction.z > 0 ? 0 : 180;
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }

    private void ReceiveAttack(EnemyController source)
    {
        Debug.Log("Pourquoi je vis, pourquoi je meurs; Pourquoi je ris, pourquoi je pleure; Voici le SOS, d'un terrien en d�tresse.");
        Destroy(gameObject);
    }
}
